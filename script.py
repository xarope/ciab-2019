from flask import Flask, request
import json
import codecs
import requests
import random
import logging
import sys
from requests.auth import HTTPBasicAuth
from bs4 import BeautifulSoup
from flask import jsonify
logging.basicConfig(level=logging.DEBUG)
# Your baseUrl

baseUrl = "http://10.96.15.93"

# Auth code - get it in your CAS controller
# you can get it by running this command:
# cat /opt/sas/viya/config/etc/SASSecurityCertificateFramework/tokens/consul/default/client.token
consul = '743aeda5-742d-4954-9a7b-bad24a632d66';

username = "sasdemo"
password = "Orion123"
client_secret = password

# Saved Project 
# Replace it with the same model name you published to MAS

savedProject = 'simulaemprestimo1_0'

app = Flask(__name__, static_url_path='/static')	

@app.route('/')
def main():
	f = codecs.open("index.html", 'r', 'utf-8')
	document = f.read()
	template = """{0}"""
	template = template.format(document)
	return template

def auth():

	urlToken1 = baseUrl + "/SASLogon/oauth/clients/consul?callback=false&serviceId=app"
	tokenFinal = ''
	headers = {
	    "X-Consul-Token": consul
	}

	try:
		firstToken = requests.post(urlToken1, headers=headers).json()["access_token"]
	except requests.exceptions.RequestException as e: 
	    print(e)

	
	urlToken2 = baseUrl + "/SASLogon/oauth/clients"

	headers2 = {
	    "Content-Type" : "application/json",
	    "Authorization": "Bearer " + firstToken
	}

	body = {"client_id": "usuario_oath", "client_secret": client_secret, "scope": ["openid", "*"], "resource_ids": "none", "authorities": ["uaa.none"], "authorized_grant_types": ["password"],"access_token_validity": 36000}

	try:
		secToken = requests.post(urlToken2, data = json.dumps(body), headers=headers2)
	except requests.exceptions.RequestException as e: 
	    print(e)

	urlToken3 = baseUrl + "/SASLogon/oauth/token?grant_type=password&username=" + username + "&password=" + password

	headers3 = {
	    "Content-Type" : "application/x-www-form-urlencoded",
	    "Accept": "application/json"
	}

	try:
		tokenFinal = requests.get(urlToken3, headers=headers3, auth=HTTPBasicAuth('usuario_oath', password)).json()["access_token"]
		print(tokenFinal)
	except requests.exceptions.RequestException as e: 
	    print(e)

	return tokenFinal

@app.route('/process', methods=['POST'])
def view_do_something():

	if request.method == 'POST':
		
		tokenFinal = auth()
		url = baseUrl + '/microanalyticScore/modules/'+ savedProject +'/steps/execute'
		dt = request.json
		print(dt)
		headers = {
		    "Content-Type": "application/json",
		    "Authorization": "Bearer " + tokenFinal}
		r = requests.post(url, data=json.dumps(dt), headers=headers).json()
		
		return str(r["outputs"]).replace("'", '"')

	else:
		return "NO OK"

if __name__ == '__main__':
	#app.run()
	app.debug = True
	app.run(host = '10.104.30.115',port=5005)